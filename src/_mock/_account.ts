// @types
import { User } from 'src/@types/account';
// config
import { HOST_API } from '../../config';

// ----------------------------------------------------------------------

export const JWT_SECRET = 'minimal-secret-key';

export const JWT_EXPIRES_IN = '3 days';

export const users: User[] = [
  {
    id: '8864c717-587d-472a-929a-8e5f298024da-0',
    displayName: 'Jaydon Frankie',
    email: 'demo@minimals.cc',
    password: 'demo1234',
    photoURL: `${HOST_API}/assets/images/avatars/avatar_default.jpg`,
    phoneNumber: '+40 777666555',
    country: 'United States',
    address: '90210 Broadway Blvd',
    state: 'California',
    city: 'San Francisco',
    zipCode: '94116',
    about: 'Praesent turpis. Phasellus viverra nulla ut metus varius laoreet. Phasellus tempus.',
    role: 'admin',
    isPublic: true,
  },
  {
    id: '8864c717-587d-472a-929a-8e5f298024da-1',
    displayName: 'Administrador Geral',
    email: 'admin@minimals.cc',
    password: 'demo1234',
    photoURL: `${HOST_API}/assets/images/avatars/avatar_1.jpg`,
    phoneNumber: '+40 777666555',
    country: 'United States',
    address: '90210 Broadway Blvd',
    state: 'California',
    city: 'San Francisco',
    zipCode: '94116',
    about: "Praesent turpis. Phasellus viverra nulla ut metus varius laoreet. Phasellus tempus.",
    role: 'admin',
    isPublic: true,
  },
  {
    id: '8864c717-587d-472a-929a-8e5f298024da-1',
    displayName: 'Coordenador Finanbank',
    email: 'coordenador@minimals.cc',
    password: 'demo1234',
    photoURL: `${HOST_API}/assets/images/avatars/avatar_1.jpg`,
    phoneNumber: '+40 777666555',
    country: 'United States',
    address: '90210 Broadway Blvd',
    state: 'California',
    city: 'San Francisco',
    zipCode: '94116',
    about: "Praesent turpis. Phasellus viverra nulla ut metus varius laoreet. Phasellus tempus.",
    role: 'coordinator',
    isPublic: true,
  },
  { 
    id: '8864c717-587d-472a-929a-8e5f298024da-2',
    displayName: 'Supervisor Crispin Dutra',
    email: 'supervisor@minimals.cc',
    password: 'demo1234',
    photoURL: `${HOST_API}/assets/images/avatars/avatar_2.jpg`,
    phoneNumber: '+40 777666555',
    country: 'United States',
    address: '90210 Broadway Blvd',
    state: 'California',
    city: 'San Francisco',
    zipCode: '94116',
    about: "Praesent turpis. Phasellus viverra nulla ut metus varius laoreet. Phasellus tempus.",
    role: 'supervisor',
    isPublic: true,
  },
];
